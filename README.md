# Simple REST service using Kerberos authentication (Spring Kerberos)

Service exposes REST endpoint that uses Kerberos authentication (Spring Kerberos).


1. Install vagrant (e.g. ```sudo apt install vagrant```)
2. Install required plugins

```./install-vagrant-plugins.sh```

3. Build app

```
mvn clean package
```

## Server

1. Run server vm 

```vagrant up server && vagrant ssh server```

2. Start app

```sudo ./run-app.sh```

## Client

1. Run client vm

```vagrant up client && vagrant ssh client```

2. Obtain Kerberos TGT for user01 

```sudo kinit user01```

3. Send request by using curl - it will return 200 OK (TGT for user01 is present) 

```sudo ./test.sh```

4. Destroy Kerberos TGT

```sudo kdestroy```

5. Send request by using curl - as there is no TGT now it will return 401 Unauthorized

```sudo ./test.sh```


## Notes
To see current Kerberos TGT:

```sudo klist -l```

```
   Principal name                 Cache name
   --------------                 ----------
   user01@EXAMPLE.COM             FILE:/tmp/krb5cc_0
```

VMs are configured to login by using su with Kerberos, e.g.

````su user01````

(provide user01 Kerberos password)

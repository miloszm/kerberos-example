#!/bin/bash
KERBEROS_CONF_SRC=/vagrant/kerberos-conf

# KDC config
cp -n /var/kerberos/krb5kdc/kdc.conf /var/kerberos/krb5kdc/kdc.conf.old
cp ${KERBEROS_CONF_SRC}/kdc.conf /var/kerberos/krb5kdc/kdc.conf

# KRB config
cp -n /etc/krb5.conf /etc/krb5.conf.old
cp ${KERBEROS_CONF_SRC}/krb5.conf /etc/krb5.conf

# KADM config
cp -n /var/kerberos/krb5kdc/kadm5.acl /var/kerberos/krb5kdc/kadm5.acl.old
cp ${KERBEROS_CONF_SRC}/kadm5.acl /var/kerberos/krb5kdc/kadm5.acl

# Remove existing db (just in case it exists)
kdb5_util -r EXAMPLE.COM destroy

# Create database (normally -P shouldn't be used)
/usr/sbin/kdb5_util create -s -P 12345

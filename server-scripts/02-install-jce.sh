#!/bin/bash
# Download & Unzip
wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jce/8/jce_policy-8.zip"
unzip jce_policy-8.zip

# Backup old jars
cp -n /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.161-5.b14.fc27.x86_64/jre/lib/security/local_policy.jar .backup
cp -n /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.161-5.b14.fc27.x86_64/jre/lib/security/US_export_policy.jar .backup

# Update JCE
cp -v /vagrant/UnlimitedJCEPolicyJDK8/*.jar /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.161-5.b14.fc27.x86_64/jre/lib/security/
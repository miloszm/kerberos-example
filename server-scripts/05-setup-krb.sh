#!/bin/bash
kadmin.local -q "addprinc root/admin"

kadmin.local -q "addprinc user01"

kadmin.local addprinc -randkey HTTP/server.example.com@EXAMPLE.COM

rm /etc/krb5.keytab

kadmin.local ktadd -k /etc/krb5.keytab HTTP/server.example.com@EXAMPLE.COM

cp /etc/krb5.keytab /tmp
chmod 644 /tmp/krb5.keytab

# if you want to authenticate as user01 by using 'su'
useradd user01
authconfig --enablekrb5 --update
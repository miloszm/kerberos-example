package pl.mma;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Optional;

@RestController
public class ApiController {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @ResponseBody
    String hello(Principal principal) {
        LOG.info("GET /hello principal={}", principal.getName());
        return "Hello " + Optional.ofNullable(principal.getName()).orElse("<unknown>") + "!";
    }

    @RequestMapping(value = "/hello-public", method = RequestMethod.GET)
    @ResponseBody
    String helloPublic() {
        LOG.info("GET /hello-public");
        return "Hello!";
    }

}

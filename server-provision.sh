#!/usr/bin/env bash
DIR=/vagrant/server-scripts
${DIR}/01-install-krb.sh
${DIR}/02-install-jce.sh
${DIR}/03-initial-setup-krb.sh
${DIR}/04-start-krb.sh
${DIR}/05-setup-krb.sh
#!/bin/bash -e
KERBEROS_CONF_SRC=/vagrant/kerberos-conf
cp -n /etc/krb5.conf /etc/krb5.conf.old
cp ${KERBEROS_CONF_SRC}/krb5.conf /etc/krb5.conf

# if you want to authenticate as user01 by using 'su'
useradd user01
authconfig --enablekrb5 --update

